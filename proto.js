//----------------string prototypes
String.prototype.trim = function () {
    return this.replace(/^s+|s+$/g, "");
};

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

String.prototype.replaceAll = function (search, replace) {
    if (replace === undefined) {
        return this.toString();
    }
    return this.replace(new RegExp('[' + search + ']', 'g'), replace);
};

String.prototype.format = function (values) {
    var regex = /\{([\w-]+)(?:\:([\w\.]*)(?:\((.*?)?\))?)?\}/g;
    var getValue = function (key) {
        if (values == null || typeof values === 'undefined') return null;
        var value = values[key];
        var type = typeof value;
        return type === 'string' || type === 'number' ? value : null;
    };

    return this.replace(regex, function (match) {
        //match will look like {sample-match}
        //key will be 'sample-match';
        var key = match.substr(1, match.length - 2);
        var value = getValue(key);
        return value != null ? value : match;
    });
};

//----------------object
Object.prototype.getAllMethods = function () {
    var memberArray = new Array();
    for (var method in this) {
        if (typeof this[method] == 'function') {
            memberArray.push(method);
        };
    };
    return memberArray;
};

Object.prototype.getOwnMethods = function () {
    var memberArray = new Array();
    for (var method in this) {
        if (typeof this[method] == 'function' && this.hasOwnProperty(method)) {
            memberArray.push(method);
        };
    };
    return memberArray;
};

//----------------array
// insert element at index
Array.prototype.insertAt = function (element, index) {
    this.splice(index, 0, element);
}

// delete element from index
Array.prototype.removeAt = function (index) {
    this.splice(index, 1);
}

Array.prototype.contains = function (obj) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === obj) return i;
    }
    return -1;
}


//---------------REGEX
function validateEmail(email) {
    if (/^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/.test(email)) {
        return (true);
    }
    alert("Invalid e-mail address! Please enter again carefully!.");
    return (false);
}